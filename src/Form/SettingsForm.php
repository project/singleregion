<?php declare(strict_types = 1);

namespace Drupal\singleregion\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Single Region settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'singleregion_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['singleregion.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['target_top'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set link target to <em>_top</em>'),
      '#description' => $this->t('If your content is used in iframes, you may want all links to open in the top frame. See MDN for <a href="@target">more information about the target attribute</a>.', [
        '@target' => 'https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a#target',
      ]),
      '#default_value' => $this->config('singleregion.settings')->get('target_top'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('singleregion.settings')
      ->set('target_top', $form_state->getValue('target_top'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
