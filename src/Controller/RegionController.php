<?php declare(strict_types = 1);

namespace Drupal\singleregion\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Single Region routes.
 */
final class RegionController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function __invoke($region): array {

    $build['content'] = [];

    return $build;
  }

  public function getTitle($region) {
    return $this->t($region);
  }

}
