## INTRODUCTION

The Single Region module allows individual regions to be displayed separately on their own page.

The primary use case for this module is to allow region content to be used within an iframe outside of a Drupal site.

However, it is important to be aware of restrictions around content within iframes, and that the behaviour of links may not be as expected.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Enable the module
- Assign the 'view single region content' permission to the required roles
- Visit /singleregion/{region}, where {region} is the machine name of the region in your site's theme, e.g. /singleregion/header

You may need to add additional conditions into the page templates for your theme to prevent wrapper markup for empty regions being displayed.

## MAINTAINERS

Current maintainers for Drupal 10:

- Malcolm Young (malcomio) - https://www.drupal.org/u/malcomio

